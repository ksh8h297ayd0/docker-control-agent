#!/usr/bin/env bash
if [[ $1 == "" ]]; then
  echo "Set settings for Control-Agent!"
  exit 0
fi
decrypted=$(echo "$1" | base64 --decode)
IFS=';' read control_agent_settings SOCKD_USER_NAME SOCKD_USER_PASSWORD <<<$decrypted

printf "NODE_ENV=production\nCONTROL_AGENT_SETTINGS=$control_agent_settings\nPROXY_LOGIN=$SOCKD_USER_NAME\nPROXY_PASSWORD=$SOCKD_USER_PASSWORD\n" >.env

docker-compose down && docker-compose up -d --build && reboot
